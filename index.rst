Documentation RestAPI Aladom
============================

.. toctree::
   :maxdepth: 2

   intro
   offers/job_offer
   contact/candidacy
   contact/contact_quote
   quotation/quote_request
   quotation/lead_delivery
   directory/training