Offres d'emploi
===============

L'API d'offres d'emploi est disponible sur les endpoints suivant :

- https://www.aladom.fr/restapi/offers/job-offer/my/ pour vos propres offres emploi
- https://www.aladom.fr/restapi/offers/job-offer/ pour l'ensemble des offres d'emploi

Lister et manipuler vos offres d'emploi
---------------------------------------

1. Lister vos offres d'emploi : ``GET /restapi/offers/job-offer/my/``
2. Consulter une offre d'emploi : ``GET /restapi/offers/job-offer/my/<id>/``
3. Ajouter une offre d'emploi : ``POST /restapi/offers/job-offer/my/``
4. Modifier une offre d'emploi : ``PATCH /restapi/offers/job-offer/my/<id>/``
5. Activer une offre d'emploi : ``POST /restapi/offers/job-offer/my/<id>/activate/``
6. Désactiver une offre d'emploi : ``POST /restapi/offers/job-offer/my/<id>/deactivate/``
7. Republier une offre d'emploi : ``POST /restapi/offers/job-offer/my/<id>/republish/``

Pour obtenir le détail des champs disponibles pour les services 1 à 4, vous
pouvez envoyer une requête ``OPTIONS`` sur ``/restapi/offers/job-offer/my/`` :

.. code-block:: bash

   $ curl -XOPTIONS https://www.aladom.fr/restapi/offers/job-offer/my/

.. code-block:: json

   {
       "name": "My Job Offer List",
       "description": "",
       "renders": [
           "application/json",
           "text/html"
       ],
       "parses": [
           "application/json",
           "application/x-www-form-urlencoded",
           "multipart/form-data"
       ],
       "actions": {
           "POST": {
               "id": {
                   "type": "integer",
                   "required": false,
                   "read_only": true,
                   "label": "Id"
               },
               "subcategory": {
                   "type": "field",
                   "required": true,
                   "read_only": false,
                   "label": "Sous-catégorie",
                   "help_text": "Vous pouvez entrer l'ID de la sous-catégorie sous forme d'entier, ou bien l'alias (slug) sous forme textuelle."
               },
               "city": {
                   "type": "field",
                   "required": true,
                   "read_only": false,
                   "label": "Ville",
                   "help_text": "Vous pouvez entrer l'ID de la ville sous forme d'entier, le code INSEE sous forme textuelle, ou bien l'alias (slug) sous forme textuelle."
               },
               "provider": {
                   "type": "integer",
                   "required": true,
                   "read_only": false,
                   "label": "Annonceur"
               },
               "title": {
                   "type": "string",
                   "required": true,
                   "read_only": false,
                   "label": "Titre",
                   "max_length": 255
               },
               "content": {
                   "type": "string",
                   "required": true,
                   "read_only": false,
                   "label": "Description de l'offre"
               },
               "profile": {
                   "type": "string",
                   "required": false,
                   "read_only": false,
                   "label": "Description du profil recherché"
               },
               "latitude": {
                   "type": "float",
                   "required": false,
                   "read_only": false,
                   "label": "Latitude"
               },
               "longitude": {
                   "type": "float",
                   "required": false,
                   "read_only": false,
                   "label": "Longitude"
               },
               "published_on": {
                   "type": "datetime",
                   "required": false,
                   "read_only": true,
                   "label": "Publiée le"
               },
               "state": {
                   "type": "choice",
                   "required": false,
                   "read_only": true,
                   "label": "État"
               },
               "hourly_wage": {
                   "type": "float",
                   "required": false,
                   "read_only": false,
                   "label": "Salaire horaire (brut)"
               },
               "wage": {
                   "type": "json",
                   "required": false,
                   "read_only": false,
                   "label": "info salaire",
                   "wage_type": {
                       "type": "multiple choice",
                       "required": false,
                       "read_only": false,
                       "label": "Type de salaire",
                       "choices": [
                           {
                               "value": 1,
                               "display_name": "Horaire"
                           },
                           {
                               "value": 2,
                               "display_name": "Mensuel"
                           },
                           {
                               "value": 3,
                               "display_name": "Annuel"
                           },
                       ]
                   },
                   "wage": {
                       "type": "float",
                       "required": false,
                       "read_only": false,
                       "label": "Salaire",
                   },
               },
               "application_email": {
                   "type": "email",
                   "required": false,
                   "read_only": false,
                   "label": "E-mail pour candidater",
                   "max_length": 255
               },
               "application_phone": {
                   "type": "string",
                   "required": false,
                   "read_only": false,
                   "label": "Téléphone pour candidater",
                   "max_length": 20
               },
               "weekly_hours": {
                   "type": "integer",
                   "required": false,
                   "read_only": false,
                   "label": "Nombre d'heures par semaine",
                   "min_value": 0,
                   "max_value": 65535
               },
               "start_date": {
                   "type": "date",
                   "required": false,
                   "read_only": false,
                   "label": "Début du contrat"
               },
               "is_part_time": {
                   "type": "boolean",
                   "required": false,
                   "read_only": false,
                   "label": "Temps partiel"
               },
               "is_full_time": {
                   "type": "boolean",
                   "required": false,
                   "read_only": false,
                   "label": "Temps plein"
               },
               "vehicle_required": {
                   "type": "boolean",
                   "required": false,
                   "read_only": false,
                   "label": "Véhicule obligatoire"
               },
               "minimum_experience": {
                   "type": "integer",
                   "required": false,
                   "read_only": false,
                   "label": "Années d'expérience requises",
                   "min_value": 0,
                   "max_value": 65535
               },
               "minimum_age": {
                   "type": "integer",
                   "required": false,
                   "read_only": false,
                   "label": "Âge minimum",
                   "min_value": 0,
                   "max_value": 65535
               },
               "contract_duration": {
                   "type": "integer",
                   "required": false,
                   "read_only": false,
                   "label": "Durée du contrat",
                   "help_text": "En nombre de mois. Obligatoire si CDD",
                   "min_value": 0,
                   "max_value": 65535
               },
               "cv_required": {
                   "type": "boolean",
                   "required": false,
                   "read_only": false,
                   "label": "CV requis"
               },
               "is_adhoc_mission": {
                   "type": "boolean",
                   "required": false,
                   "read_only": false,
                   "label": "Mission ponctuelle"
               },
               "adhoc_mission_hours": {
                   "type": "float",
                   "required": false,
                   "read_only": false,
                   "label": "Durée de la mission en heures"
               },
               "adhoc_mission_price": {
                   "type": "float",
                   "required": false,
                   "read_only": false,
                   "label": "Salaire pour la mission"
               },
               "pole_emploi_id": {
                   "type": "integer",
                   "required": false,
                   "read_only": true,
                   "label": "Id pole emploi"
               },
               "pole_emploi_state": {
                   "type": "string",
                   "required": false,
                   "read_only": true,
                   "label": "Statut pole emploi"
               },
               "pole_emploi_reject_reason": {
                   "type": "string",
                   "required": false,
                   "read_only": true,
                   "label": "Raison de rejet pole emploi"
               },
               "origin_reference": {
                   "type": "string",
                   "required": false,
                   "read_only": false,
                   "label": "Reference de l'offre",
                   "max_length": 30
               },
               "origin_source": {
                   "type": "string",
                   "required": false,
                   "read_only": false,
                   "label": "Source de l'offre",
                   "max_length": 255
               },
               "contract_types": {
                   "type": "multiple choice",
                   "required": false,
                   "read_only": false,
                   "label": "Types de contrat",
                   "choices": [
                       {
                           "display_name": "CDI",
                           "value": 1
                       },
                       {
                           "display_name": "CDD",
                           "value": 2
                       },
                       {
                           "display_name": "Intérim",
                           "value": 3
                       },
                       {
                           "display_name": "Stage",
                           "value": 4
                       },
                       {
                           "display_name": "Apprentissage",
                           "value": 5
                       },
                       {
                           "display_name": "Autre",
                           "value": 6
                       }
                   ]
               },
               "url": {
                   "type": "field",
                   "required": false,
                   "read_only": true,
                   "label": "Url"
               }
           }
       }
   }

.. _fields:

Champs disponibles
------------------

Cette section décrit les champs disponibles pour les services suivants :

- :ref:`list`
- :ref:`get`
- :ref:`create`
- :ref:`update`

id
##

- **Type :** integer
- **Lecture seule**
- **Nom :** Id

Ce champ n'est pas à renseigner lors de la création d'une offre d'emploi. Vous
pouvez le stocker dans votre SI de manière à pouvoir récupérer cette offre par
la suite et effectuer des mises à jours.

subcategory
###########

- **Type :** special
- **Obligatoire**
- **Nom :** Sous-catégorie

En lecture, ce champ se présente sous la forme d'un dictionnaire contenant les
clés suivantes :

- ``id`` l'ID de la catégorie
- ``slug`` l'alias de la catégorie
- ``name`` le nom de la catégorie

En écriture, vous pouvez soit préciser l'ID de la sous-catégorie, soit l'alias
(slug).

Pour obtenir la liste exhaustive des catégories disponibles, envoyez une
requête ``GET`` sur ``/restapi/offers/subcategory/``.

.. code-block:: bash

   $ curl -XGET https://www.aladom.fr/restapi/offers/subcategory/


.. code-block:: json

   [
       {
           "id": 1,
           "name": "Aide aux personnes handicap\u00e9es",
           "slug": "aide-personnes-handicapees",
           "category": "Aide au handicap",
           "category_id": 1
       },
       {
           "id": 2,
           "name": "Langue des signes",
           "slug": "langue-des-signes",
           "category": "Aide au handicap",
           "category_id": 1
       }
   ]

city
####

- **Type :** special
- **Obligatoire**
- **Nom :** Ville

En lecture, ce champ se présente sous la forme d'un dictionnaire contenant les
clés suivantes :

- ``id`` l'ID de la ville
- ``slug`` l'alias de la ville
- ``code`` le code INSEE de la ville
- ``name`` le nom de la ville

En écriture, vous pouvez soit préciser l'ID de la ville sous forme d'**entier**,
soit l'alias (slug), soit le code INSEE sous forme de **chaine de caractères**.

La méthode recommandée est d'utiliser le code INSEE. Si vous ne le stockez pas
déjà dans votre SI, il existe des API publiques comme OpenDataSoft permettant
de récupérer ces codes avec le code postal ou/et le nom de la ville.

Sinon, vous pouvez tenter de "deviner" l'alias à partir du nom de la ville
stocké dans votre SI. Il s'agit du nom de la ville écrit en minuscules, sans
accents ni caractères spéciaux et en remplaçant les espaces par des tirets.
Attention notamment aux villes dont l'orthographe est ambigu (notamment
l'utilisation de "saint" ou "st"). Certaines villes de France ont le même nom
et donc le même "slug". Pour éviter les ambiguités, vous pouvez rajouter le
numéro de département. Par exemple "saint-nazaire-44", "saint-denis-974",
"bourg-en-bresse-01", "ajaccio-2A"...

.. warning:: Attention, si vous utilisez le code INSEE, pensez bien à l'envoyer
             sous forme de chaîne de caractères. Si vous l'envoyez sous forme
             d'entier, il sera considéré comme étant l'ID de la ville dans
             notre SI.

provider
########

- **Type :** integer
- **Obligatoire**
- **Nom :** Annonceur

ID du compte membre auquel doit être associé l'offre d'emploi. La liste des
membres gérés par votre compte peut être obtenu par une requête ``GET`` sur
``/restapi/user/member/my/managed_users/``.

.. code-block:: bash

   $ curl -XGET https://www.aladom.fr/restapi/user/member/my/managed_users/


.. code-block:: json

   [
       {
           "id": 123456,
           "city": {
               "code": "35238",
               "slug": "rennes",
               "id": 13811,
               "name": "Rennes"
           },
           "email": "jean.dupont@example.com",
           "first_name": "Jean",
           "last_name": "Dupont",
           "phone": "0612345678",
           "co_trade_name": "Agence Example Rennes",
           "url": "http://www.aladom.fr/annuaire/pro/123456/",
       }
   ]

title
#####

- **Type :** string
- **Obligatoire**
- **Nom :** Titre
- **Taille max :** 255

content
#######

- **Type :** string
- **Obligatoire**
- **Nom :** Description de l'offre

profile
#######

- **Type :** string
- **Facultatif**
- **Nom :** Description du profil recherché

latitude
########

- **Type :** float
- **Facultatif**
- **Nom :** Latitude

longitude
#########

- **Type :** float
- **Facultatif**
- **Nom :** Longitude

published_on
############

- **Type :** datetime
- **Lecture seule**
- **Nom :** Publiée le

state
#####

- **Type :** choice
- **Lecture seule**
- **Nom :** État
- **Choix :**
   - ``1`` En attente de modération
   - ``2`` Active
   - ``4`` Inactive

hourly_wage
###########

- **Type :** float
- **Facultatif**
- **Nom :** Salaire horaire (brut)

wage
####

- **Type :** json (wage_type/wage)
- **Facultatif**
- **Nom :** Information salaire
- **Valeur :**
    - **wage**
        - **Type :** float
        - **Nom :** Salaire
    - **wage_type**
        - **Type :** float
        - **Nom :** Type Salaire
        - **Choix :**
           - ``1`` Horaire
           - ``2`` Mensuel
           - ``3`` Annuel
- **Exemple :**
```json
{"wage": 1653.50, "wage_type": 2}
```

application_email
#################

- **Type :** email
- **Facultatif**
- **Nom :** E-mail pour candidater
- **Taille max :** 255

application_phone
#################

- **Type :** string
- **Facultatif**
- **Nom :** Téléphone pour candidater
- **Taille max :** 20

weekly_hours
############

- **Type :** integer
- **Facultatif**
- **Nom :** Nombre d'heures par semaine

start_date
##########

- **Type :** date
- **Facultatif**
- **Nom :** Début du contrat

is_part_time
############

- **Type :** boolean
- **Facultatif**
- **Nom :** Temps partiel

is_full_time
############

- **Type :** boolean
- **Facultatif**
- **Nom :** Temps plein

vehicle_required
################

- **Type :** boolean
- **Facultatif**
- **Nom :** Véhicule obligatoire

minimum_experience
##################

- **Type :** integer
- **Facultatif**
- **Nom :** Années d'expérience requises

minimum_age
###########

- **Type :** integer
- **Facultatif**
- **Nom :** Âge minimum

contract_duration
#################

- **Type :** integer
- **Facultatif**
- **Nom :** Durée du contrat

En nombre de mois. **Obligatoire si CDD.**

cv_required
###########

- **Type :** boolean
- **Facultatif**
- **Nom :** CV requis

is_adhoc_mission
################

- **Type :** boolean
- **Facultatif**
- **Nom :** Mission ponctuelle

adhoc_mission_hours
###################

- **Type :** float
- **Facultatif**
- **Nom :** Durée de la mission en heures

adhoc_mission_price
###################

- **Type :** float
- **Facultatif**
- **Nom :** Salaire pour la mission

pole_emploi_id
##############

- **Type :** integer
- **Lecture seule**
- **Nom :** ID pôle emploi

pole_emploi_state
#################

- **Type :** string
- **Lecture seule**
- **Nom :** Statut pôle emploi

pole_emploi_reject_reason
#########################

- **Type :** string
- **Lecture seule**
- **Nom :** Raison de rejet pole emploi

origin_reference
################

- **Type :** string
- **Facultatif**
- **Nom :** Référence de l'offre

origin_source
#############

- **Type :** string
- **Facultatif**
- **Nom :** Source de l'offre

contract_types
##############

- **Type :** multiple choice
- **Facultatif**
- **Nom :** types de contrat
- **Choix :**
   - ``1`` CDI
   - ``2`` CDD
   - ``3`` Intérim
   - ``4`` Stage
   - ``5`` Apprentissage
   - ``6`` Autre

url
###

- **Type :** string
- **Lecture seule**
- **Nom :** URL

.. _list:

Lister vos offres d'emploi
--------------------------

Permet de lister vos offres d'emploi.

.. code-block:: bash

   $ curl -XGET https://www.aladom.fr/restapi/offers/job-offer/my/

:ref:`Détail des champs <fields>`

.. _get:

Consulter une offre d'emploi
----------------------------

Permet de récupérer une offre d'emploi en connaissant son ID.

.. code-block:: bash

   $ curl -XGET https://www.aladom.fr/restapi/offers/job-offer/my/123456/

:ref:`Détail des champs <fields>`

.. _create:

Ajouter une offre d'emploi
--------------------------

Permet de nous envoyer une de vos offres d'emploi.

.. code-block:: bash

   $ curl -XPOST -d '{"title": "Exemple", ...}' https://www.aladom.fr/restapi/offers/job-offer/my/

:ref:`Détail des champs <fields>`

.. _update:

Modifier une offre d'emploi
---------------------------

Permet de modifier une de vos offres d'emploi.

.. code-block:: bash

   $ curl -XPATCH -d '{"title": "Exemple", ...}' https://www.aladom.fr/restapi/offers/job-offer/my/123456/

:ref:`Détail des champs <fields>`

.. _activate:

Activer une offre d'emploi
--------------------------

Permet d'activer une offre d'emploi que vous aviez désactivée.

.. code-block:: bash

   $ curl -XPOST https://www.aladom.fr/restapi/offers/job-offer/my/123456/activate/

.. _deactivate:

Désactiver une offre d'emploi
-----------------------------

Permet désactiver une de vos offres d'emploi.

.. code-block:: bash

   $ curl -XPOST https://www.aladom.fr/restapi/offers/job-offer/my/123456/deactivate/

.. _republish:

Republier une offre d'emploi
----------------------------

Permet de republier une de vos offres d'emploi.

.. code-block:: bash

   $ curl -XPOST https://www.aladom.fr/restapi/offers/job-offer/my/123456/republish/

Lister l'ensemble des offres d'emploi
-------------------------------------

Ce endpoint vous permet de lister les offres d'emploi actives sur Aladom.

Exemple :

.. code-block:: bash

   $ curl -XGET https://www.aladom.fr/restapi/offers/job-offer/

Vous pouvez également filter en fonction:

1. De la ville ou l'offre est disponible
    - Slug : ``GET /restapi/offers/job-offer/?city=rennes``
    - Code insee : ``GET /restapi/offers/job-offer/?city=35238``
    - ID : ``GET /restapi/offers/job-offer/?city_id=13811``
2. Du département
    - ``GET /restapi/offers/job-offer/?subarea=35``
3. La sous-catégorie de l'offre
    - Slug : ``GET /restapi/offers/job-offer/?subcategory=menage``
    - ID : ``GET /restapi/offers/job-offer/?subcategory_id=33``
4. Du type d'annonceur
    - Option Premium : ``GET /restapi/offers/job-offer/?provider_is_premium=True``
    - Particuliers : ``GET /restapi/offers/job-offer/?provider_user_type=1``
    - Professionnels : ``GET /restapi/offers/job-offer/?provider_user_type=2``
    - Assocations : ``GET /restapi/offers/job-offer/?provider_user_type=3``
5. Du type de contrat
    - CDI : ``GET /restapi/offers/job-offer/?contract_type=1``
    - CDD : ``GET /restapi/offers/job-offer/?contract_type=2``
    - Intérim : ``GET /restapi/offers/job-offer/?contract_type=3``
    - Stage : ``GET /restapi/offers/job-offer/?contract_type=4``
    - Apprentissage : ``GET /restapi/offers/job-offer/?contract_type=5``
    - Autre : ``GET /restapi/offers/job-offer/?contract_type=6``
6. De la possibilité de diffuser cette annonce
    - ``GET /restapi/offers/job-offer/?broadcasting_allowed=True``

.. warning:: Si vous utilisez cette API dans le but de diffuser les offres
             d'emploi d'Aladom, merci de bien veillez à utiliser le filtre
             **broadcasting_allowed=True** afin d'exclure les offres ne pouvant
             pas être rediffusées contractuellement.
