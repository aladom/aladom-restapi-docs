Contact directs
===============

Cette API n'est pas encore disponible en dehors du webhook.

Webhook
-------

Si vous avez mis en place un webhook (Cf. :ref:`webhooks`) vous recevrez les
contacts directs directement sur votre webhook sous le nom d'événement :
``new_contact_quote``.

Le champ ``type`` sera toujours ``contact.Contact``. Verifiez cependant la
valeur et ignorez l'événement si la valeur n'est pas correcte. Cela évitera
des erreurs si nous venions à ajouter des types de données dans le futur.

Exemple de données reçues
#########################

.. code-block:: json

   {
       "event": "new_candidacy",
       "type": "contact.Candidacy",
       "object": {
           "id": 1234567,
           "message": "Bonjour,\r\nJe souhaiterai apprendre l'anglais durant mon congé parental, j'ai un niveau faux debutant. \r\nJe suis sur la commune du francois.\r\nPouvez vous me donner votre tarif et conditions?\r\nMerci\r\nJeanne",
           "origin_email": "jeanne.durand@hotmail.fr",
           "origin_name": "Jeanne Durand",
           "origin_phone": "0123456789",
           "posted_on": "2017-07-30T22:00:17",
           "service_offer": {
               "city_insee": "97209",
               "id": 12345,
               "myaladom_url": "http://www.aladom.fr/monaladom/mes-annonces/service/cours-anglais/edit",
               "provider": {
                   "co_trade_name": null,
                   "email": "jean.dupont@exemple.fr",
                   "id": 123456
               },
               "subcategory": "cours-anglais",
               "title": "Cours d'anglais tous niveaux",
               "url": "http://www.aladom.fr/cours-anglais/fort-de-france-972/cours-danglais-tous-niveaux-9ix"
           }
       }
   }
