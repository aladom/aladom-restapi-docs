Candidatures
============

L'API des candidatures est disponible sur le endpoint suivant :

https://www.aladom.fr/restapi/contact/candidacy/

Ce endpoint permet de lister les candidatures reçues pour vos offres d'emploi et d'ajouter des candidatures externes

Lister et manipuler vos candidatures
------------------------------------

1. Ajouter une candidature : ``POST /restapi/contact/candidacy/``
2. Lister vos candidatures : ``GET /restapi/contact/candidacy/``


Ajouter une candidature
-----------------------

Voici un exemple d'envoi

.. code-block:: bash

   $ curl -XPOST -H'Content-Type:application/json' https://www.aladom.fr/restapi/contact/candidacy/ -d '{"subcategory": 33, "provider": 11111, "origin_email": "prenom.nom@email.com", "origin_phone": "0606060606", "first_name": "Prénom", "last_name": "Nom", "civility": 1, "cv_file": "JVBERi0xLjMKJcTl8uXrp/Og0....."}'

.. code-block:: json

   {
	    "id": 1400050,
	    "experience": null,
	    "contract_type": "",
	    "has_cv": false,
	    "cv_url": "/utilisateur/aaaa/cv",
	    "cv_extension": ".pdf",
	    "origin_name": "Prénom Nom",
	    "origin_email": "prenom.nom@email.com",
	    "origin_phone": "0606060606",
	    "message": "",
	    "posted_on": "2022-10-20T16:25:25.527430",
	    "job_offer": null,
	    "birth_date": null,
	    "address": "",
	    "city": null,
	    "has_vehicle": null,
	    "degrees": [],
	    "filters": [],
	    "first_name": "Prénom",
	    "last_name": "Nom",
	    "status": 0,
	}


Champs disponibles pour l'ajout
-------------------------------

Cela peut évoluer. Si des champs ne sont présents ici mais sont pertinents selon vous, n'hésitez pas à nous contacter.


first_name
##########

- **Type :** string
- **Nom :** Prénom du candidat

last_name
#########

- **Type :** string
- **Nom :** Nom du candidat

origin_email
############

- **Type :** email
- **Nom :** Adresse e-mail du candidat

origin_phone
############

- **Type :** string
- **Nom :** Numéro de téléphone du candidat

provider
########

- **Type :** integer
- **Nom :** Agence/Structure (sur lequel vous allez déposer la candidature)

cv_file
#######

- **Type :** base64
- **Nom :** Fichier CV

subcategory
###########

- **Type :** integer
- **Nom :** Sous-catégorie

message (facultatif)
####################

- **Type :** string
- **Nom :** Message laissé par le candidat


posted_on (facultatif)
######################

- **Type :** datetime (2022-10-20 16:25)
- **Nom :** Date de la candidature

job_offer (facultatif)
######################

- **Type :** integer
- **Nom :** Offre d'emploi


Précision : Si vous fournissez un ID Offre d'emploi (job_offer), vous n'êtes pas obligé de fournir les 2 champs ``subcategory`` et ``provider``. Depuis l'offre d'emploi, nous pourrons récupérer ses 2 champs. Par conséquent, la candidature ne sera pas considérée comme une candidature externe sur votre ATS.


Lister vos candidatures
-----------------------

.. code-block:: bash

   $ curl -XGET https://www.aladom.fr/restapi/contact/candidacy/

.. code-block:: json

   {
       "count": 123,
       "next": "http://192.168.0.51:8000/restapi/contact/candidacy/?limit=100&offset=100",
       "previous": null,
       "results": [
           {
               "address": null,
               "birth_date": "1999-05-05",
               "city": {
                   "code": "76351",
                   "id": 31283,
                   "latitude": 49.490002,
                   "longitude": 0.1,
                   "name": "Le Havre",
                   "postal_codes": [
                       "76600",
                       "76610",
                       "76620"
                   ],
                   "slug": "le-havre",
                   "subarea_id": "76"
               },
               "contract_type": "un emploi \u00e0 temps plein",
               "cv_url": null,
               "degrees": [
                   {
                       "id": 67,
                       "name": "Licence en math\u00e9matiques",
                       "slug": "licence-en-mathematiques"
                   }
               ],
               "filters": [
                   {
                       "id": 80,
                       "question_label": "Pouvez-vous faire du soutien scolaire aux enfants ?",
                       "cv_label": "Je donne des cours aux enfants"
                   }
               ],
               "experience": 6,
               "first_name": "Jean",
               "has_cv": false,
               "has_vehicle": false,
               "id": 123456,
               "job_offer": {
                   "city_insee": "76351",
                   "id": 1234567,
                   "origin_reference": "abc123",
                   "provider": {
                       "co_trade_name": "ACME",
                       "email": "contact@acme.fr",
                       "id": 765432
                   },
                   "subcategory": "cours-francais",
                   "title": "Cours particuliers en FRANCAIS en classe de 1ERE"
               },
               "last_name": "Dupont",
               "message": "Bonjour,\r\n\r\nSuite \u00e0 l'offre d'emploi de cours de fran\u00e7ais que vous proposez, je me permets de vous transmettre ma candidature.\r\nMerci de me contacter si vous souhaitez obtenir plus de renseignements.\r\n\r\nCordialement,\r\n \r\n\r\n",
               "origin_email": "jean.dupont@gmail.com",
               "origin_name": "Jean Dupont",
               "origin_phone": null,
               "posted_on": "2017-07-29T23:14:05",
               "member": {
                   "civility": 1,
                   "picture_url": null,
                   "job_status": 2
               }
           },
           {
           },
           {
           }
       ]
   }


Champs disponibles sur la liste
-------------------------------

id
##

- **Type :** integer
- **Nom :** Id

experience
##########

- **Type :** integer
- **Nom :** Années d'expérience

contract_type
#############

- **Type :** string
- **Nom :** Type de contrat

has_cv
######

- **Type :** boolean
- **Nom :** A joint son CV

cv_url
######

- **Type :** string
- **Nom :** URL du CV


Précision : Pour accéder au CV, vous devez rajouter ``https://www.aladom.fr`` avant l'URL fourni puis faire un GET avec votre token pour vous authentifier. Si l'URL sur le JSON est ``"/utilisateur/aaaa/cv"``, cela donne :


.. code-block:: bash

   $ curl -H "Authorization: Token {token}" -XGET https://www.aladom.fr/utilisateur/aaaa/cv


origin_name
###########

- **Type :** string
- **Nom :** Nom complet du candidat

origin_email
############

- **Type :** email
- **Nom :** Adresse e-mail du candidat

origin_phone
############

- **Type :** string
- **Nom :** Numéro de téléphone du candidat

message
#######

- **Type :** string
- **Nom :** Message laissé par le candidat

posted_on
#########

- **Type :** datetime
- **Nom :** Date de la candidature

job_offer
#########

- **Type :** nested object
- **Nom :** Offre d'emploi

Contient les informations de l'offre d'emploi concernée

birth_date
##########

- **Type :** date
- **Nom :** Date de naissance du candidat

address
#######

- **Type :** string
- **Nom :** Adresse du candidat (rue)

city
####

- **Type :** nested object
- **Nom :** Ville du candidat

Contient les informations de la ville du candidat

- ``code`` le code INSEE
- ``id`` l'ID dans notre SI
- ``latitude`` latitude du centre de la ville
- ``longitude`` longitude du centre de la ville
- ``name`` nom de la ville
- ``postal_codes`` liste des codes postaux
- ``slug`` identifiant textuel de la ville
- ``subarea_id`` numéro de département

has_vehicle
###########

- **Type :** boolean
- **Nom :** Le candidat est-il véhiculé

``null`` signifiant "non renseigné"

degrees
#######

- **Type :** list
- **Nom :** Liste des diplômes du candidat

Chaque élément de la liste est un dictionnaire contenant les clés suivantes :

- ``id`` ID du diplôme dans notre SI
- ``name`` dénomination du diplôme en français
- ``slug`` identifiant textuel du diplôme

filters
#######

- **Type :** list
- **Nom :** Liste des compétences du candidat

Chaque élément de la liste est un dictionnaire contenant les clés suivantes :

- ``id`` ID de la compétence dans notre SI
- ``question_label`` question type posé au candidat
- ``cv_label`` intitulé de la compétence

first_name
##########

- **Type :** string
- **Nom :** Prénom du candidat

last_name
##########

- **Type :** string
- **Nom :** Nom de famille du candidat

member
######

- **Type :** nested object
- **Nom :** Informations complémentaires du candidat

Contient les informations du candidat

- ``civility`` civilité du candidat (1: Monsieur / 2: Madame)
- ``picture_url`` Url de la photo de profil du candidat
- ``job_status`` Situation professionnelle

Valeurs de ``job_status``

``0`` Non renseigné
``1`` Étudiant
``2`` Salarié
``3`` Demandeur d'emploi
``4`` Retraité
``5`` Autre

Webhook
-------

Si vous avez mis en place un webhook (Cf. :ref:`webhooks`) vous recevrez les
nouvelles candidatures directement sur votre webhook sous le nom d'événement :
``new_candidacy``.

Le champ ``type`` sera toujours ``contact.Candidacy``. Verifiez cependant la
valeur et ignorez l'événement si la valeur n'est pas correcte. Cela évitera
des erreurs si nous venions à ajouter des types de données dans le futur.

Exemple de données reçues
#########################

.. code-block:: json

   {
       "event": "new_candidacy",
       "type": "contact.Candidacy",
       "object": {
           "address": null,
           "birth_date": "1993-03-30",
           "city": {
               "code": "29019",
               "id": 10916,
               "latitude": 48.390835,
               "longitude": -4.485556,
               "name": "Brest",
               "postal_codes": ["29200"],
               "slug": "brest",
               "subarea_id": "29"
           },
           "contract_type": "un job étudiant",
           "cv_url": null,
           "degrees": [],
           "experience": 0,
           "first_name": "Jean",
           "has_cv": false,
           "has_vehicle": false,
           "id": 123456,
           "job_offer": {
               "city_insee": "29019",
               "id": 1234567,
               "origin_reference": null,
               "provider": {
                   "co_trade_name": null,
                   "email": "jean.dupont@exemple.fr",
                   "id": 370180
               },
               "subcategory": "cours-arabe",
               "title": "Cours d'arabe"
           },
           "last_name": "Dupont",
           "message": "Bonjour, je suis un étudiant à brest et j'aimerai bien donner des cours d'arabe pour tous .",
           "origin_email": "jean.dupont@exemple.fr",
           "origin_name": "Jean Dupont",
           "origin_phone": "0123456789",
           "posted_on": "2017-07-30T22:42:11"
       }
   }
