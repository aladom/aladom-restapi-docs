
Formations
==========

L'API Formations est disponible sur l'endpoint suivant :

- https://www.aladom.fr/restapi/directory/training/my/ pour vos propres formations

Lister et manipuler vos formations
----------------------------------

1. Lister vos formations : ``GET /restapi/directory/training/my/``
2. Consulter une formation : ``GET /restapi/directory/training/my/<id>/``
3. Ajouter une formation: ``POST /restapi/directory/training/my/``
4. Modifier une formation : ``PATCH /restapi/directory/training/my/<id>/``
5. Activer une formation: ``POST /restapi/directory/training/my/<id>/activate/``
6. Désactiver une formation : ``POST /restapi/directory/training/my/<id>/deactivate/``

Pour obtenir le détail des champs disponibles, vous
pouvez envoyer une requête ``OPTIONS`` sur ``/restapi/directory/training/my/`` :

.. code-block:: bash

   $ curl -XOPTIONS https://www.aladom.fr/restapi/directory/training/my/

.. code-block:: json

    {
        "name": "My Training List",
        "description": "",
        "renders": [
            "application/json",
            "text/html"
        ],
        "parses": [
            "application/json",
            "application/x-www-form-urlencoded",
            "multipart/form-data"
        ],
        "actions": {
            "POST": {
                "id": {
                    "type": "integer",
                    "required": false,
                    "read_only": true,
                    "label": "Id"
                },
                "subcategory": {
                    "type": "field",
                    "required": true,
                    "read_only": false,
                    "label": "Sous-catégorie",
                    "help_text": "Vous pouvez entrer l'ID de la sous-catégorie sous forme d'entier, ou bien l'alias (slug) sous forme textuelle."
                },
                "member": {
                    "type": "field",
                    "required": true,
                    "read_only": false,
                    "label": "Centre de formation"
                },
                "title": {
                    "type": "string",
                    "required": true,
                    "read_only": false,
                    "label": "Titre",
                    "max_length": 255
                },
                "slug": {
                    "type": "field",
                    "required": false,
                    "read_only": true,
                    "label": "slug"
                },
                "intro": {
                    "type": "string",
                    "required": true,
                    "read_only": false,
                    "label": "Présentation"
                },
                "description": {
                    "type": "string",
                    "required": false,
                    "read_only": false,
                    "label": "Programme"
                },
                "requirements": {
                    "type": "string",
                    "required": false,
                    "read_only": false,
                    "label": "Pré-requis"
                },
                "email": {
                    "type": "email",
                    "required": false,
                    "read_only": false,
                    "label": "E-mail",
                    "max_length": 255
                },
                "published_on": {
                    "type": "datetime",
                    "required": false,
                    "read_only": false,
                    "label": "Publiée le"
                },
                "state": {
                    "type": "choice",
                    "required": false,
                    "read_only": false,
                    "label": "État",
                    "choices": [
                        {
                            "value": 2,
                            "display_name": "Actif"
                        },
                        {
                            "value": 3,
                            "display_name": "Inactif"
                        },
                        {
                            "value": 4,
                            "display_name": "Supprimé"
                        }
                    ]
                },
                "hits": {
                    "type": "integer",
                    "required": false,
                    "read_only": true,
                    "label": "Visites"
                },
                "degree": {
                    "type": "field",
                    "required": false,
                    "read_only": false,
                    "label": "Diplômes"
                },
                "cpf_funding": {
                    "type": "boolean",
                    "required": false,
                    "read_only": false,
                    "label": "Financement CPF"
                },
                "vae_eligibility": {
                    "type": "boolean",
                    "required": false,
                    "read_only": false,
                    "label": "Éligibilité VAE"
                },
                "admission_level": {
                    "type": "choice",
                    "required": false,
                    "read_only": false,
                    "label": "Niveau d'admission",
                    "choices": [
                        {
                            "value": 1,
                            "display_name": "CAP, BEP"
                        },
                        {
                            "value": 2,
                            "display_name": "BAC"
                        },
                        {
                            "value": 3,
                            "display_name": "BAC+2"
                        },
                        {
                            "value": 4,
                            "display_name": "BAC+3"
                        },
                        {
                            "value": 5,
                            "display_name": "BAC+4"
                        },
                        {
                            "value": 6,
                            "display_name": "BAC+5"
                        },
                        {
                            "value": 7,
                            "display_name": "BAC+8"
                        }
                    ]
                },
                "success_percentage": {
                    "type": "integer",
                    "required": false,
                    "read_only": false,
                    "label": "Taux de réussite",
                    "min_value": -2147483648,
                    "max_value": 2147483647
                },
                "next_session_date": {
                    "type": "date",
                    "required": false,
                    "read_only": false,
                    "label": "Date de la prochaine session"
                },
                "next_session_anytime": {
                    "type": "boolean",
                    "required": false,
                    "read_only": false,
                    "label": "Prochaine session à tout moment"
                },
                "duration_time": {
                    "type": "float",
                    "required": false,
                    "read_only": false,
                    "label": "Durée"
                },
                "duration_time_unity": {
                    "type": "choice",
                    "required": false,
                    "read_only": false,
                    "label": "Temporalité",
                    "choices": [
                        {
                            "value": 1,
                            "display_name": "heure(s)"
                        },
                        {
                            "value": 2,
                            "display_name": "jour(s)"
                        },
                        {
                            "value": 3,
                            "display_name": "mois"
                        }
                    ]
                },
                "duration_customized": {
                    "type": "boolean",
                    "required": false,
                    "read_only": false,
                    "label": "Durée sur mesure"
                },
                "price": {
                    "type": "float",
                    "required": false,
                    "read_only": false,
                    "label": "Prix"
                },
                "audiences": {
                    "type": "multiple choice",
                    "required": false,
                    "read_only": false,
                    "label": "Audiences",
                    "choices": [
                        {
                            "value": 1,
                            "display_name": "Salarié en poste"
                        },
                        {
                            "value": 2,
                            "display_name": "Demandeur d'emploi"
                        },
                        {
                            "value": 3,
                            "display_name": "Entreprise"
                        },
                        {
                            "value": 4,
                            "display_name": "Étudiant"
                        }
                    ]
                },
                "places": {
                    "type": "multiple choice",
                    "required": false,
                    "read_only": false,
                    "label": "Place",
                    "choices": [
                        {
                            "value": 1,
                            "display_name": "En centre"
                        },
                        {
                            "value": 2,
                            "display_name": "En entreprise"
                        },
                        {
                            "value": 3,
                            "display_name": "À distance"
                        },
                        {
                            "value": 4,
                            "display_name": "En alternance"
                        }
                    ]
                },
                "videos": {
                    "type": "field",
                    "required": false,
                    "read_only": false,
                    "label": "Videos"
                },
                "images": {
                    "type": "field",
                    "required": false,
                    "read_only": false,
                    "label": "Images"
                },
                "url": {
                    "type": "field",
                    "required": false,
                    "read_only": true,
                    "label": "Url"
                }
            }
        }
    }

.. _fields_training:

Champs disponibles
------------------

Cette section décrit les champs disponibles pour les services suivants :

- :ref:`list_training`
- :ref:`get_training`
- :ref:`create_training`
- :ref:`update_training`
- :ref:`activate_training`
- :ref:`deactivate_training`

id
##

- **Type :** integer
- **Lecture seule**
- **Nom :** Id

Ce champ n'est pas à renseigner lors de la création d'une offre d'emploi. Vous
pouvez le stocker dans votre SI de manière à pouvoir récupérer cette offre par
la suite et effectuer des mises à jours.

subcategory
###########

- **Type :** special
- **Obligatoire**
- **Nom :** Sous-catégorie

En lecture, ce champ se présente sous la forme d'un dictionnaire contenant les
clés suivantes :

- ``id`` l'ID de la catégorie
- ``slug`` l'alias de la catégorie
- ``name`` le nom de la catégorie

En écriture, vous pouvez soit préciser l'ID de la sous-catégorie, soit l'alias
(slug).

Pour obtenir la liste exhaustive des catégories disponibles, envoyez une
requête ``GET`` sur ``/restapi/offers/subcategory/``.

.. code-block:: bash

   $ curl -XGET https://www.aladom.fr/restapi/offers/subcategory/


.. code-block:: json

   [
       {
           "id": 1,
           "name": "Aide aux personnes handicap\u00e9es",
           "slug": "aide-personnes-handicapees",
           "category": "Aide au handicap",
           "category_id": 1
       },
       {
           "id": 2,
           "name": "Langue des signes",
           "slug": "langue-des-signes",
           "category": "Aide au handicap",
           "category_id": 1
       }
 ]


member
######

- **Type :** integer ou nom centre formation ou email
- **Obligatoire**
- **Nom :** Centre de formation

ID du compte membre auquel doit être associé la formation. La liste des
membres gérés par votre compte peut être obtenu par une requête ``GET`` sur
``/restapi/user/member/my/managed_users/``.

.. code-block:: bash

   $ curl -XGET https://www.aladom.fr/restapi/user/member/my/managed_users/


.. code-block:: json

   [
       {
           "id": 123456,
           "city": {
               "code": "35238",
               "slug": "rennes",
               "id": 13811,
               "name": "Rennes"
           },
           "email": "jean.dupont@example.com",
           "first_name": "Jean",
           "last_name": "Dupont",
           "phone": "0612345678",
           "co_trade_name": "Agence Example Rennes",
           "url": "http://www.aladom.fr/annuaire/pro/123456/",
       }
   ]

title
#####

- **Type :** string
- **Obligatoire**
- **Nom :** Titre
- **Taille max :** 255

slug
####

- **Type :** string
- **Lecture seule**
- **Nom :** slug

intro
#####

- **Type :** string
- **Obligatoire**
- **Nom :** Présentation de la formation

description
###########

- **Type :** string
- **Obligatoire**
- **Nom :** Programme de la formation

requirements
############

- **Type :** string
- **Obligatoire**
- **Nom :** Pré-requis de la formation

email
#####

- **Type :** string
- **Facultatif**
- **Nom :** email contact

published_on
############

- **Type :** datetime
- **Lecture seule**
- **Nom :** Publiée le

state
#####

- **Type :** choice
- **Lecture seule**
- **Nom :** État
- **Choix :**
   - ``2`` Active
   - ``3`` Inactive
   - ``4`` Supprimé

hits
####

- **Type :** integer
- **Lecture seule**
- **Nom :** Nombre de clique sur la formation

degree
######

- **Type :** Degree
- **Facultatif**
- **Nom :** Diplômes

cpf_funding
###########

- **Type :** boolean
- **Facultatif**
- **Nom :** Financement CPF

vae_eligibility
###############

- **Type :** boolean
- **Facultatif**
- **Nom :** éligibilité VAE

admission_level
###############

- **Type :** choice
- **Facultatif**
- **Nom :** niveau d'admission
- **Choix :**
   - ``1`` CAP, BEP
   - ``2`` BAC
   - ``3`` BAC+2
   - ``4`` BAC+3
   - ``5`` BAC+4
   - ``6`` BAC+5
   - ``7`` BAC+8

success_percentage
##################

- **Type :** integer
- **Facultatif**
- **Nom :** taux de réussite

next_session_date
#################

- **Type :** date
- **Facultatif**
- **Nom :** date de la prochaine session

next_session_anytime
####################

- **Type :** boolean
- **Facultatif**
- **Nom :** prochaine session à tout moment

duration_time
#############

- **Type :** float
- **Facultatif**
- **Nom :** durée

duration_time_unity
###################

- **Type :** choice
- **Facultatif**
- **Nom :** temporalité
- **Choix :**
   - ``1`` heure(s)
   - ``2`` jour(s)
   - ``3`` mois

duration_customized
###################

- **Type :** boolean
- **Facultatif**
- **Nom :** durée sur mesure

price
#####

- **Type :** float
- **Facultatif**
- **Nom :** prix

audiences
#########

- **Type :** multiple choice (liste)
- **Facultatif**
- **Nom :** Public admis
- **Choix :**
   - ``1`` Salarié en poste
   - ``2`` Demandeur d'emploi
   - ``3`` Entreprise
   - ``4`` Étudiant
- **Exemple :**  [2, 4]


places
######

- **Type :** multiple choice (liste)
- **Facultatif**
- **Nom :** Lieu
- **Choix :**
   - ``1`` En centre
   - ``2`` En entreprise
   - ``3`` À distance
   - ``4`` En alternance
- **Exemple :**  [1, 2]


videos
######

- **Type :** liste
- **Facultatif**
- **Nom :** vidéos
- **Max :** 3
- **Exemple :**  ["https://www.youtube.com/embed/aHPWyAyGYi0?si=BOedZNWSYmXMi6Ua", "..."]



Exemple json
------------

.. code-block:: json

    {
        "subcategory": 20,
        "member": 1697636,
        "title": "title",
        "intro": "intro",
        "description": "description.",
        "requirements": "requirements",
        "cpf_funding": true,
        "vae_eligibility": false,
        "admission_level": 1,
        "success_percentage": null,
        "next_session_date": null,
        "next_session_anytime": false,
        "duration_time": null,
        "duration_time_unity": 1,
        "duration_customized": false,
        "price": 3500.0,
        "audiences": [2,3],
        "places": [1,2],
        "videos": ["https://www.youtube.com"]
    }


.. _list_training:

Lister vos formations
---------------------

Permet de lister vos formations.

.. code-block:: bash

   $ curl -XGET https://www.aladom.fr/restapi/directory/training/my/

:ref:`Détail des champs <fields_training>`

.. _get_training:

Consulter une formation
-----------------------

Permet de récupérer une formation en connaissant son ID.

.. code-block:: bash

   $ curl -XGET https://www.aladom.fr/restapi/directory/training/my/123456/

:ref:`Détail des champs <fields_training>`

.. _create_training:

Ajouter une formation
---------------------

Permet de nous envoyer une de vos formations.

.. code-block:: bash

   $ curl -XPOST -d '{"title": "Exemple", ...}' https://www.aladom.fr/restapi/directory/training/my/

:ref:`Détail des champs <fields_training>`

.. _update_training:

Modifier une formation
----------------------

Permet de modifier une de vos formations.

.. code-block:: bash

   $ curl -XPATCH -d '{"title": "Exemple", ...}' https://www.aladom.fr/directory/training/my/123456/

:ref:`Détail des champs <fields_training>`

.. _activate_training:

Activer une formation
---------------------

Permet d'activer une formation que vous aviez désactivée.

.. code-block:: bash

   $ curl -XPOST https://www.aladom.fr/restapi/directory/training/my/123456/activate/

.. _deactivate_training:

Désactiver une formation
------------------------

Permet désactiver une de vos formation.

.. code-block:: bash

   $ curl -XPOST https://www.aladom.fr/restapi/directory/training/my/123456/deactivate/


Webhooks et questions
---------------------

Si vous avez besoin qu'on mette en place un webhook pour que vous recevez vos demandes d'information de formation, ou si vous avez une question, vous pouvez nous contactez ici : dev@aladom.fr. 
