.. _quote-request-api:

Demandes de devis
=================

L'API de demandes de devis se sépare en différents endpoints selon le type
de demande, chaque type définissant différents champs.

- Ménage https://www.aladom.fr/restapi/quotation/quote-request/cleaning/
- Aménagement du domicile https://www.aladom.fr/restapi/quotation/quote-request/home-furnishings/
- Bricolage/Jardinage https://www.aladom.fr/restapi/quotation/quote-request/crafts/
- Aide aux publics fragiles https://www.aladom.fr/restapi/quotation/quote-request/help/
- Garde d'enfants https://www.aladom.fr/restapi/quotation/quote-request/childcare/
- Cours de musique https://www.aladom.fr/restapi/quotation/quote-request/music/
- Soutien scolaire https://www.aladom.fr/restapi/quotation/quote-request/tutoring/

Il existe cependant un tronc commun définit par la clé ``quote_request`` qui
est un objet imbriqué.

.. note:: Les endpoints listés ci-dessus ne concernent que l'envoie de leads
          vers notre base de données. Si vous souhaitez obtenir la liste des
          leads que vous avez reçu, il faut utiliser l'API :ref:`lead-delivery`.
          Vous pouvez également mettre en place un webhook (Cf
          :ref:`quote-request-webhook`) pour recevoir les leads dès qu'ils vous
          sont envoyés.

Pour obtenir la liste des champs d'un endpoint, envoyez une requête ``OPTIONS``
sur ce endpoint.

.. code-block:: bash

   $ curl -XOPTIONS https://www.aladom.fr/restapi/quotation/quote-request/cleaning/

.. code-block:: json

   {
       "name": "Cleaning Quote Request List",
       "description": "",
       "renders": [
           "application/json",
           "text/html"
       ],
       "parses": [
           "application/json",
           "application/x-www-form-urlencoded",
           "multipart/form-data"
       ],
       "actions": {
           "POST": {
               "subcategory": {
                   "type": "choice",
                   "required": true,
                   "read_only": false,
                   "label": "Sous-cat\u00e9gorie",
                   "choices": [
                       {
                           "value": "menage",
                           "display_name": "menage"
                       },
                       {
                           "value": "repassage",
                           "display_name": "repassage"
                       }
                   ]
               },
               "quote_request": {
                   "type": "nested object",
                   "required": true,
                   "read_only": false,
                   "label": "Quote request",
                   "children": {
                       "id": {
                           "type": "integer",
                           "required": false,
                           "read_only": true,
                           "label": "Id"
                       },
                       "city": {
                           "type": "field",
                           "required": true,
                           "read_only": false,
                           "label": "Ville",
                           "help_text": "Vous pouvez entrer l'ID de la ville sous forme d'entier, le code INSEE sous forme textuelle, ou bien l'alias (slug) sous forme textuelle."
                       },
                       "civility": {
                           "type": "choice",
                           "required": false,
                           "read_only": false,
                           "label": "Civilit\u00e9",
                           "choices": [
                               {
                                   "value": 2,
                                   "display_name": "Monsieur"
                               },
                               {
                                   "value": 1,
                                   "display_name": "Madame"
                               }
                           ]
                       },
                       "first_name": {
                           "type": "string",
                           "required": false,
                           "read_only": false,
                           "label": "Pr\u00e9nom",
                           "max_length": 255
                       },
                       "last_name": {
                           "type": "string",
                           "required": false,
                           "read_only": false,
                           "label": "Nom",
                           "max_length": 255
                       },
                       "email": {
                           "type": "email",
                           "required": true,
                           "read_only": false,
                           "label": "E-mail",
                           "max_length": 255
                       },
                       "address": {
                           "type": "string",
                           "required": false,
                           "read_only": false,
                           "label": "Adresse",
                           "max_length": 255
                       },
                       "phone": {
                           "type": "string",
                           "required": false,
                           "read_only": false,
                           "label": "T\u00e9l\u00e9phone",
                           "max_length": 20
                       },
                       "phone_alt": {
                           "type": "string",
                           "required": false,
                           "read_only": false,
                           "label": "T\u00e9l\u00e9phone alternatif",
                           "max_length": 20
                       },
                       "weekly_nb_hours": {
                           "type": "integer",
                           "required": false,
                           "read_only": false,
                           "label": "Nombre d'heures",
                           "min_value": 0,
                           "max_value": 65535
                       },
                       "frequency": {
                           "type": "choice",
                           "required": false,
                           "read_only": false,
                           "label": "Fr\u00e9quence d'intervention",
                           "choices": [
                               {
                                   "value": 1,
                                   "display_name": "Intervention ponctuelle"
                               },
                               {
                                   "value": 2,
                                   "display_name": "Toutes les semaines"
                               },
                               {
                                   "value": 3,
                                   "display_name": "Tous les 15 jours"
                               },
                               {
                                   "value": 4,
                                   "display_name": "Tous les mois"
                               },
                               {
                                   "value": 5,
                                   "display_name": "\u00c0 d\u00e9terminer"
                               }
                           ]
                       },
                       "start_date": {
                           "type": "string",
                           "required": false,
                           "read_only": false,
                           "label": "D\u00e9but de la prestation",
                           "max_length": 255
                       }
                   }
               },
               "intervention_type": {
                   "type": "choice",
                   "required": false,
                   "read_only": false,
                   "label": "Type d'intervention",
                   "choices": [
                       {
                           "value": 1,
                           "display_name": "M\u00e9nage chez un particulier"
                       },
                       {
                           "value": 2,
                           "display_name": "Repassage chez un particulier"
                       },
                       {
                           "value": 3,
                           "display_name": "M\u00e9nage et repassage chez un particulier"
                       },
                       {
                           "value": 4,
                           "display_name": "M\u00e9nage/repassage et lavage de vitres chez un particulier"
                       },
                       {
                           "value": 5,
                           "display_name": "Lavage de vitres chez un particulier"
                       },
                       {
                           "value": 6,
                           "display_name": "M\u00e9nage de bureaux"
                       },
                       {
                           "value": 7,
                           "display_name": "M\u00e9nage d'une copropri\u00e9t\u00e9/syndic"
                       },
                       {
                           "value": 8,
                           "display_name": "M\u00e9nage d'un local commercial ou profession lib\u00e9rale"
                       },
                       {
                           "value": 9,
                           "display_name": "Nettoyage de vitres de bureaux"
                       }
                   ]
               },
               "lodgment": {
                   "type": "choice",
                   "required": false,
                   "read_only": false,
                   "label": "Type de logement",
                   "choices": [
                       {
                           "value": 1,
                           "display_name": "Appartement"
                       },
                       {
                           "value": 2,
                           "display_name": "Maison"
                       }
                   ]
               },
               "surface": {
                   "type": "integer",
                   "required": false,
                   "read_only": false,
                   "label": "Superficie",
                   "help_text": "en m\u00b2",
                   "min_value": 0,
                   "max_value": 65535
               },
               "nb_staff": {
                   "type": "integer",
                   "required": false,
                   "read_only": false,
                   "label": "Nombre d'employ\u00e9s",
                   "min_value": 0,
                   "max_value": 65535
               },
               "has_contract": {
                   "type": "boolean",
                   "required": false,
                   "read_only": false,
                   "label": "Contrat en cours avec une autre soci\u00e9t\u00e9"
               },
               "operator_comment": {
                   "type": "string",
                   "required": false,
                   "read_only": false,
                   "label": "Commentaire opérateur",
               }
           }
       }
   }

.. warning:: La suite de ce document reprend les informations données par la
             méthode ``OPTIONS`` de manière plus lisible. Cependant, il est
             préférable de vous fier aux informations données par la méthode
             ``OPTIONS``, le délai de mise à jour de cette documentation
             pouvant varier.


Vérification de l'acceptabilité d'un lead
-----------------------------------------

Il est possible de vérifier si un lead est acceptable en envoyant 
en POST une requête JSON au endpoint suivant:
https://www.aladom.fr/restapi/quotation/quote-request/is_acceptable/

Le corps de la requête est composé de la sous-catégorie et de l'email du lead à vérifier.

.. code-block:: json

   {
       "subcategory": "menage",
       "email": "jean.dupont@example.com"
   }

Si le lead est acceptable le service renvoie la réponse JSON suivante:

.. code-block:: json

   {
       "success": true
   }

Sinon:

.. code-block:: json

   {
       "success": false,
       "error": "Nous avons déja reçu une demande de ce prospect dans les 30 derniers jours"
   }


.. _tronc-commun:

Tronc commun
------------

Cette section décrit les champs présents dans l'objet imbriqué
``quote_request`` présent dans chaque endpoint:

id
##

- **Type :** integer
- **Lecture seule**
- **Nom :** Id

Ce champ n'est pas à renseigner lors de la création d'un lead. Vous pouvez le
stocker dans votre SI de manière à pouvoir nous le communiquer en cas problème
relatif à ce lead.

city
####

- **Type :** string
- **Obligatoire**
- **Nom :** Ville

Ce champ est destiné à recevoir le code INSEE de la ville de la demande.

civility
########

- **Type :** choice
- **Facultatif**
- **Nom :** Civilité
- **Choix :**
   - ``1`` Madame
   - ``2`` Monsieur

Civilité de la personne à l'origine de la demande.

first_name
##########

- **Type :** string
- **Facultatif**
- **Nom :** Prénom
- **Taille max :** 255

Prénom de la personne à l'origine de la demande.

last_name
#########

- **Type :** string
- **Facultatif**
- **Nom :** Nom
- **Taille max :** 255

Nom de la personne à l'origine de la demande.

email
#####

- **Type :** string
- **Obligatoire**
- **Nom :** E-mail
- **Taille max :** 255

E-mail de la personne à l'origine de la demande.

address
#######

- **Type :** string
- **Facultatif**
- **Nom :** Adresse
- **Taille max :** 255

Rue de la personne à l'origine de la demande.

phone
#####

- **Type :** string
- **Obligatoire**
- **Nom :** Téléphone
- **Taille max :** 20

Numéro de téléphone de la personne à l'origine de la demande.

phone_alt
#########

- **Type :** string
- **Facultatif**
- **Nom :** Téléphone alternatif
- **Taille max :** 20

Autre numéro de téléphone de la personne à l'origine de la demande.

weekly_nb_hours
###############

- **Type :** integer
- **Facultatif**
- **Nom :** Nombre d'heures
- **Valeur min :** 0
- **Valeur max :** 65535

Nombre d'heures souhaité par semaine

frequency
#########

- **Type :** choice
- **Facultatif**
- **Nom :** Fréquence d'intervention
- **Choix :**
   - ``1`` Intervention ponctuelle
   - ``2`` Toutes les semaines
   - ``3`` Tous les 15 jours
   - ``4`` Tous les mois
   - ``5`` À déterminer

start_date
##########

- **Type :** string
- **Facultatif**
- **Nom :** Début de la prestation
- **Taille max :** 255

Début de la prestation souhaité sous forme de texte libre.

.. _cleaning:

Ménage
------

subcategory
###########

- **Type :** choice
- **Obligatoire**
- **Nom :** Sous-catégorie
- **Choix :**
   - ``"menage"`` Ménage
   - ``"repassage"`` Repassage

quote_request
#############

- **Type :** nested object
- **Obligatoire**
- **Nom :** Quote request
- Cf. :ref:`tronc-commun`

intervention_type
#################

- **Type :** choice
- **Facultatif**
- **Nom :** Type d'intervention
- **Choix :**
   - ``1`` Ménage chez un particulier
   - ``2`` Repassage chez un particulier
   - ``3`` Ménage et repassage chez un particulier
   - ``4`` Ménage/repassage et lavage de vitres chez un particulier
   - ``5`` Lavage de vitres chez un particulier
   - ``6`` Ménage de bureaux
   - ``7`` Ménage d'une copropriété/syndic
   - ``8`` Ménage d'un local commercial ou profession libérale
   - ``9`` Nettoyage de vitres de bureaux

lodgement
#########

- **Type :** choice
- **Facultatif**
- **Nom :** Type de logement
- **Choix :**
   - ``1`` Appartement
   - ``2`` Maison

surface
#######

- **Type :** integer
- **Facultatif**
- **Nom :** Superficie
- **Valeur min :** 0
- **Valeur max :** 65535

Superficie du logement en m².

nb_staff
########

- **Type :** integer
- **Facultatif**
- **Nom :** Nombre d'employés
- **Valeur min :** 0
- **Valeur max :** 65535

Nombre d'employés dans l'entreprise le cas échéant.

has_contract
############

- **Type :** boolean
- **Facultatif**
- **Nom :** Contrat en cours avec une autre société

Le demandeur a t'il actuellement un contrat avec une autre société de ménage ?

.. _home-furnishings:

Aménagement du domicile
-----------------------

subcategory
###########

- **Type :** choice
- **Obligatoire**
- **Nom :** Sous-catégorie
- **Choix :**
   - ``"amenagement-domicile"`` Aménagement du domicile

quote_request
#############

- **Type :** nested object
- **Obligatoire**
- **Nom :** Quote request
- Cf. :ref:`tronc-commun`

first_name
##########

- **Type :** string
- **Facultatif**
- **Nom :** Prénom de la personne à aider
- **Taille max :** 255

last_name
#########

- **Type :** string
- **Facultatif**
- **Nom :** Nom de la personne à aider
- **Taille max :** 255

is_in_relationship
##################

- **Type :** boolean
- **Facultatif**
- **Nom :** Personne en couple

age
###

- **Type :** string
- **Facultatif**
- **Nom :** Âge des personnes à aider
- **Taille max :** 45

intervention_types
##################

- **Type :** multiple choice
- **Facultatif**
- **Nom :** Type d'intervention
- **Choix :**
   - ``1`` Installation d'ascenseurs
   - ``2`` Mise à hauteur du mobilier existant
   - ``3`` Elargissement de portes, pose de portes coulissantes
   - ``4`` Installation de douches extra-plates
   - ``5`` Installation d'une baignoire à porte
   - ``6`` Pose de W.C surélevés
   - ``7`` Installation de monte escalier

pathology
#########

- **Type :** string
- **Facultatif**
- **Nom :** Pathologies spécifiques

.. _crafts:

Bricolage/Jardinage
-------------------

subcategory
###########

- **Type :** choice
- **Obligatoire**
- **Nom :** Sous-catégorie
- **Choix :**
   - ``"bricolage"`` Bricolage
   - ``"jardinage"`` Jardinage

quote_request
#############

- **Type :** nested object
- **Obligatoire**
- **Nom :** Quote request
- Cf. :ref:`tronc-commun`

has_equipement
##############

- **Type :** boolean
- **Facultatif**
- **Nom :** Possède le matériel

.. _help:

Aide aux publics fragiles
-------------------------

subcategory
###########

- **Type :** choice
- **Obligatoire**
- **Nom :** Sous-catégorie
- **Choix :**
   - ``"aide-personnes handicapees"`` Aide aux personnes handicapées
   - ``"langue-des-signes"`` Langue des signes
   - ``"transport-handicape"`` Transport handicapé
   - ``"aide-aux-personnes-agees"`` Aide aux personnes âgées
   - ``"cuisine-pour-personnes-agees"`` Cuisine pour personnes âgées
   - ``"livraison-des-courses"`` Livraison des courses
   - ``"transport-personnes-agees"`` Transport personnes âgées
   - ``"famille-accueil"`` Famille d'accueil
   - ``"livraison-de-repas"`` Livraison de repas
   - ``"auxiliaire-de-vie"`` Auxiliaire de vie
   - ``"maison-retraite"`` Maison de retraite
   - ``"residence-services-seniors"`` Résidence services seniors
   - ``"usld"`` USLD
   - ``"teleassistance"`` Téléassistance

quote_request
#############

- **Type :** nested object
- **Obligatoire**
- **Nom :** Quote request
- Cf. :ref:`tronc-commun`

first_name
##########

- **Type :** string
- **Facultatif**
- **Nom :** Prénom de la personne à aider
- **Taille max :** 255

last_name
#########

- **Type :** string
- **Facultatif**
- **Nom :** Nom de la personne à aider
- **Taille max :** 255

intervention_types
##################

- **Type :** multiple choice
- **Facultatif**
- **Nom :** Type d'intervention
- **Choix :**
   - ``1`` Garde à domicile
   - ``2`` Livraison de courses
   - ``3`` Livraison / Portage de repas
   - ``4`` Préparation des repas
   - ``5`` Aide à la mobilité (courses, médecin, ...)
   - ``6`` Aide à la toilette
   - ``7`` Aide pour le ménage / repassage
   - ``8`` Aide au lever / coucher
   - ``9`` Aide à la prise de médicaments

care_time
#########

- **Type :** choice
- **Facultatif**
- **Nom :** Type de garde
- **Choix :**
   - ``1`` Journée
   - ``2`` Nuit
   - ``3`` Journée et nuit

weekdays
########

- **Type :** multiple choice
- **Facultatif**
- **Nom :** Jours de garde
- **Choix :**
   - ``1`` Lundi
   - ``2`` Mardi
   - ``3`` Mercredi
   - ``4`` Jeudi
   - ``5`` Vendredi
   - ``6`` Samedi
   - ``7`` Dimanche

age
###

- **Type :** string
- **Facultatif**
- **Nom :** Âge des personnes à aider
- **Taille max :** 45

is_in_relationship
##################

- **Type :** boolean
- **Facultatif**
- **Nom :** Personne en couple

need_care
#########

- **Type :** boolean
- **Facultatif**
- **Nom :** Besoin de soins

pathology
#########

- **Type :** string
- **Facultatif**
- **Nom :** Pathologies spécifiques

help_plan
#########

- **Type :** string
- **Facultatif**
- **Nom :** Plan d'aide

other_services
##############

- **Type :** string
- **Facultatif**
- **Nom :** Autres intervenants

Autres intervenants au domicile de la personne.

.. _childcare:

Garde d'enfants
---------------

subcategory
###########

- **Type :** choice
- **Obligatoire**
- **Nom :** Sous-catégorie
- **Choix :**
   - ``"baby-sitting"`` Baby-sitting
   - ``"garde-enfants"`` Garde d'enfants

quote_request
#############

- **Type :** nested object
- **Obligatoire**
- **Nom :** Quote request
- Cf. :ref:`tronc-commun`

nb_children
###########

- **Type :** integer
- **Facultatif**
- **Nom :** Nombre d'enfants
- **Valeur min :** 0
- **Valeur max :** 65535

ages
####

- **Type :** multiple choice
- **Facultatif**
- **Nom :** Âge des enfants
- **Choix :**
   - ``0`` 0 ans
   - ``1`` 1 an
   - ``2`` 2 ans
   - ``3`` 3 ans
   - ``4`` 4 ans
   - ``5`` 5 ans
   - ``6`` 6 ans
   - ``7`` 7 ans
   - ``8`` 8 ans
   - ``9`` 9 ans
   - ``10`` 10 ans
   - ``11`` 11 ans
   - ``12`` 12 ans
   - ``13`` 13 ans
   - ``14`` 14 ans
   - ``15`` 15 ans
   - ``16`` 16 ans
   - ``17`` 17 ans
   - ``18`` 18 ans

weekdays
########

- **Type :** multiple choice
- **Facultatif**
- **Nom :** Jours de garde
- **Choix :**
   - ``1`` Lundi
   - ``2`` Mardi
   - ``3`` Mercredi
   - ``4`` Jeudi
   - ``5`` Vendredi
   - ``6`` Samedi
   - ``7`` Dimanche

holidays_included
#################

- **Type :** boolean
- **Facultatif**
- **Nom :** Besoin pendant les vacances scolaires

intervention_type
#################

- **Type :** choice
- **Facultatif**
- **Nom :** Type d'intervention
- **Choix :**
   - ``1`` Journée
   - ``2`` Nuit
   - ``3`` Journée et nuit

vehicle_required
################

- **Type :** boolean
- **Facultatif**
- **Nom :** Véhicule obligatoire

duration
########

- **Type :** choice
- **Facultatif**
- **Nom :** Durée du besoin
- **Choix :**
   - ``0`` À déterminer
   - ``1`` Moins d'un mois
   - ``2`` Plus d'un mois
   - ``3`` Toute l'année scolaire

.. _music:

Cours de musique
----------------

subcategory
###########

- **Type :** choice
- **Obligatoire**
- **Nom :** Sous-catégorie
- **Choix :**
   - ``"cours-batterie"`` Cours de batterie
   - ``"cours-chant"`` Cours de chant
   - ``"cours-guitare"`` Cours de guitare
   - ``"cours-piano"`` Cours de piano
   - ``"cours-solfege"`` Cours de solfège
   - ``"cours-violon"`` Cours de violon
   - ``"autres-cours-musique"`` Autres cours de musique
   - ``"cours-flute"`` Cours de flûte

quote_request
#############

- **Type :** nested object
- **Obligatoire**
- **Nom :** Quote request
- Cf. :ref:`tronc-commun`

delivery_types
##############

- **Type :** multiple choice
- **Facultatif**
- **Nom :** Type de prestation
- **Choix :**
   - ``1`` À domicile
   - ``2`` Par skype
   - ``3`` Par vidéo en accès libre

intervention_type
#################

- **Type :** choice
- **Obligatoire**
- **Nom :** Type d'intervention
- **Choix :**
   - ``0`` Préparation intensive
   - ``1`` Suivi régulier pendant plusieurs mois

nb_students
###########

- **Type :** integer
- **Facultatif**
- **Nom :** Nombre de personnes concernées
- **Valeur min :** 0
- **Valeur max :** 65535

students_level
##############

- **Type :** choice
- **Facultatif**
- **Nom :** Niveau des élèves
- **Choix :**
   - ``1`` Débutant
   - ``2`` Intermédiare
   - ``3`` Confirmé

.. _tutoring:

Soutien scolaire
----------------

subcategory
###########

- **Type :** choice
- **Obligatoire**
- **Nom :** Sous-catégorie
- **Choix :**
   - ``"cours-allemand"`` Cours d'allemand
   - ``"cours-anglais"`` Cours d'anglais
   - ``"cours-arabe"`` Cours d'arabe
   - ``"cours-chinois"`` Cours de chinois
   - ``"cours-espagnol"`` Cours d'espagnol
   - ``"cours-français"`` Cours de français
   - ``"cours-italien"`` Cours d'italien
   - ``"cours-japonais"`` Cours de japonais
   - ``"cours-portugais"`` Cours de portugais
   - ``"cours-russe"`` Cours de russe
   - ``"autres-cours-langue"`` Autres cours de langue
   - ``"aide-aux-devoirs"`` Aide aux devoirs
   - ``"cours-biologie"`` Cours de biologie
   - ``"cours-dessin"`` Cours de dessin
   - ``"cours-economie"`` Cours d'économie
   - ``"cours-maths"`` Cours de maths
   - ``"cours-physique-chimie"`` Cours de physique chimie
   - ``"autres-cours-particuliers"`` Autres cours particuliers
   - ``"cours-francais-littérature"`` Cours de français et de littérature

quote_request
#############

- **Type :** nested object
- **Obligatoire**
- **Nom :** Quote request
- Cf. :ref:`tronc-commun`

intervention_type
#################

- **Type :** choice
- **Obligatoire**
- **Nom :** Type d'intervention
- **Choix :**
   - ``0`` Préparation intensive
   - ``1`` Suivi régulier pendant plusieurs mois

nb_students
###########

- **Type :** integer
- **Facultatif**
- **Nom :** Nombre de personnes concernées
- **Valeur min :** 0
- **Valeur max :** 65535

student_names
#############

- **Type :** string
- **Facultatif**
- **Nom :** Nom des élèves
- **Taille max :** 50

students_type
#############

- **Type :** choice
- **Obligatoire**
- **Nom :** Type d'élèves
- **Choix :**
   - ``1`` Primaire
   - ``2`` Collège
   - ``3`` Lycée
   - ``4`` Étudiant
   - ``5`` Adulte

students_class
##############

- **Type :** string
- **Facultatif**
- **Nom :** Classe des élèves
- **Taille max :** 100

students_level
##############

- **Type :** choice
- **Obligatoire**
- **Nom :** Niveau des élèves
- **Choix :**
   - ``1`` Débutant
   - ``2`` Intermédiare
   - ``3`` Confirmé

.. _quote-request-webhook:

Webhook
-------

Si vous avez mis en place un webhook (Cf. :ref:`webhooks`) vous recevrez les
nouveaux leads directement sur votre webhook sous le nom d'événement :
``new_lead``.

Le champ ``type`` précisera le type de demande de devis. En découleront les
champs précisés dans le champ ``object``. Liste des types possibles :

- ``quotation.CleaningQuoteRequest`` : :ref:`cleaning`
- ``quotation.HomeFurnishingsQuoteRequest`` : :ref:`home-furnishings`
- ``quotation.CraftsQuoteRequest`` : :ref:`crafts`
- ``quotation.HelpQuoteRequest`` : :ref:`help`
- ``quotation.ChildcareQuoteRequest`` :ref:`childcare`
- ``quotation.MusicQuoteRequest`` :ref:`music`
- ``quotation.TutoringQuoteRequest`` :ref:`tutoring`

Exemple de données reçues
#########################

.. code-block:: json

   {
       "event": "new_lead",
       "type": "quotation.CleaningQuoteRequest",
       "object": {
           "has_contract": null,
           "intervention_type": 3,
           "lodgment": 1,
           "nb_staff": null,
           "quote_request": {
               "address": null,
               "city": "51454",
               "civility": 2,
               "email": "jean.dupont@exemple.fr",
               "first_name": "Jean",
               "frequency": 3,
               "id": 123456,
               "last_name": "Dupont",
               "phone": "0123456789",
               "phone_alt": "",
               "start_date": "Dans moins d'un mois",
               "weekly_nb_hours": 2
           },
           "subcategory": "menage",
           "surface": 70
       }
   }


.. warning:: Si une erreur survient lors de l'envoie du lead (si votre serveur
             est indisponible par exemple), 4 nouvelles tentatives seront
             effectuées avant d'abandonner complètement. Afin de ne pas rater
             de leads, vous pouvez mettre en place une tâche cron chargée de
             récupérer les derniers leads reçus et vérifier qu'ils sont bien
             enregistrés dans votre base de données une fois par heure par
             exemple. Cf :ref:`lead-delivery`
