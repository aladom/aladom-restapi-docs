.. _lead-delivery:

Livraison de leads
==================

Cette API permet de récupérer la liste des leads que vous avez reçus. Elle est
ordonnée par date de réception décroissante.

Elle est prévue pour être utilisée en complément du webhook permettant de vous
envoyer directement les demandes de devis. En appelant régulièrement cette API,
par exemple toutes les heures ou une fois par jour, vous pourrez vous assurer
de ne pas manquer de leads si une erreur est survenue lors de l'envoie par
webhook. Cf :ref:`quote-request-webhook`

Exemple
-------

.. code-block:: bash

   $ curl -XGET https://www.aladom.fr/restapi/quotation/lead-delivery/


.. code-block:: json

   {
       "count": 2704,
       "next": "https://www.aladom.fr/restapi/quotation/lead-delivery/?limit=100&offset=100",
       "previous": null,
       "results": [
           {
               "date": "2017-07-27T15:42:35",
               "id": 123456,
               "quote_request": {
                   "accept_email": true,
                   "address": "2 quai des Orfèvres",
                   "city": {
                       "code": "87154",
                       "id": 34837,
                       "name": "Saint-Junien",
                       "slug": "saint-junien"
                   },
                   "civility": 1,
                   "comment": "",
                   "description": "<p>Demande de devis requalifi&eacute;e par t&eacute;l&eacute;phone pour une prestation <strong>de Cours de violon</strong>.</p>\r\n<p>Demande v&eacute;rifi&eacute;e par Emeline le 27 juil. 2017.</p>\r\n<p>Fourchette de prix communiqu&eacute;e et valid&eacute;e.</p>\r\n<ul>\r\n<li><strong>Contact :</strong> Madame Jeanne Dupont</li>\r\n<li><strong>T&eacute;l&eacute;phone :</strong> 01 23 45 67 89</li>\r\n<li><strong>E-mail :</strong> jeanne.dupont@example.com</li>\r\n<li><strong>Adresse :</strong> 2 Quai des Orfèvres</li>\r\n<li><strong>Ville :</strong> Saint-Junien - 87200</li>\r\n<li><strong>Type de prestation :</strong> &Agrave; domicile</li>\r\n<li><strong>Type d'intervention :</strong> Suivi r&eacute;gulier pendant plusieurs mois</li>\r\n<li><strong>Nombre de personnes :</strong> 1</li>\r\n<li><strong>Fr&eacute;quence d'intervention :</strong> Toutes les semaines</li>\r\n<li><strong>Volume horaire :</strong> 2h</li>\r\n<li><strong>Niveau :</strong> 1</li>\r\n<li><strong>D&eacute;but souhait&eacute; de la prestation :</strong> Mi-ao&ucirc;t</li>\r\n<li><strong>Horaire de rappel :</strong> La prospecte pr&eacute;f&egrave;re &ecirc;tre contact&eacute;e par mail</li>\r\n</ul>\r\n<p>Coordonn&eacute;es et demande valid&eacute;es.</p>",
                   "type": "quotation.MusicQuoteRequest",
                   "details": {
                       "delivery_types": [
                           1
                       ],
                       "intervention_type": 1,
                       "nb_students": 1,
                       "students_level": 1,
                       "subcategory": "cours-violon"
                   },
                   "email": "jeanne.dupont@example.com",
                   "first_name": "Jeanne",
                   "frequency": 2,
                   "id": 123456,
                   "last_name": "Dupont",
                   "phone": "0123456789",
                   "phone_alt": null,
                   "pro_recall_date": "La prospecte pr\u00e9f\u00e8re \u00eatre contact\u00e9e par mail",
                   "start_date": "Mi-ao\u00fbt",
                   "title": "",
                   "weekly_nb_hours": 2
               },
               "sponsor": 9999
           },
           {
           }
       ]
   }


Champs disponibles
------------------

id
##

- **Type :** integer
- **Nom :** Id

Vous pouvez stocker ce champ dans votre SI de manière à pouvoir nous le
communiquer en cas de problème avec cette transmission.

sponsor
#######

- **Type :** integer
- **Nom :** Commanditaire

Identifiant du compte auquel a été envoyé ce lead. Il vous permettra de
différencier les comptes que vous gérez si vous êtes un réseau gérant plusieurs
comptes.

date
####

- **Type :** date
- **Nom :** Date de transmission

Date à laquelle ce lead vous a été transmis.

quote_request
#############

Les champs utilisés dans cette API sont similaires aux champs décrits dans
l'API des :ref:`quote-request-api`. Les champs de premier niveau correspondent
à ceux décrits dans la partie "Tronc commun". Il y a en plus un champ
``details`` qui contiendra les informations propres à chaque type de demande de
devis. Les données décrites par ce champs sont précisées par le champ ``type``.
Voici les valeurs possibles du champ type :

- ``quotation.CleaningQuoteRequest`` : :ref:`cleaning`
- ``quotation.HomeFurnishingsQuoteRequest`` : :ref:`home-furnishings`
- ``quotation.CraftsQuoteRequest`` : :ref:`crafts`
- ``quotation.HelpQuoteRequest`` : :ref:`help`
- ``quotation.ChildcareQuoteRequest`` :ref:`childcare`
- ``quotation.MusicQuoteRequest`` :ref:`music`
- ``quotation.TutoringQuoteRequest`` :ref:`tutoring`
